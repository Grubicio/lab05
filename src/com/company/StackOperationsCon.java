package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StackOperationsCon implements StackOperations {
    private List<String> stack = new ArrayList();
    final int TOP_OF_STACK = 0;

    @Override
    public List<String> get() {
        return stack;
    }

    @Override
    public Optional<String> pop() {
        if (!stack.isEmpty()) {
            return Optional.of(stack.remove(TOP_OF_STACK));
        }
        return Optional.empty();
    }

        @Override
        public void push(String item){
            stack.add(TOP_OF_STACK,item);
        }

}
